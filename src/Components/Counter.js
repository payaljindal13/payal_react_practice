import React, {Component} from "react";
class Counter extends Component{
    constructor(){
        super()
        this.state = {
            count: 1,
            stopper: 0
        }
    }
    increment(){
        let interval = setInterval(()=>{
            if(this.state.stopper%2 === 0){
                clearInterval(interval);
                console.log(`Stopped counter at ${this.state.count}`)
                return;
            }
            this.setState(previousState => ({
                count: previousState.count + 1
            }))
            },1000)
    }

    clickHandler(){
        this.setState(previousState => ({
            stopper: previousState.stopper + 1
        }),() => {
            if(this.state.stopper%2 !== 0){
                console.log(`Started counter at ${this.state.count}`)
                this.increment();
           }
        })
    }
    render(){
        return(
            <div>
            <h2>Count: {this.state.count}</h2>
            <button onClick={()=>this.clickHandler()}>Start</button>
            </div>
            
        )
    }
}

export default Counter
Collapse
