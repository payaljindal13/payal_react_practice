import React from 'react'
const Welcome = (props) => {
    return (
        <div>
        <h2>Welcome {props.name}</h2>
        <h4>{props.children}</h4>
        </div>
    )
}

export default Welcome