import logo from './logo.svg';
import './App.css';
import Click from './Components/Click.js'
import Message from './Components/Message.js'
import Counter from './Components/Counter.js'
import ParentComponent from './Components/ParentComponent.js';
import NameList from './Components/NameList';
function App() {
  return (
    <div className="App">
      {// <h2>Hello World</h2>
      // <Greet name="Mac">
      // <button>This is child</button></Greet>
      // <Greet name="Harry">
      // <p>This is child of Harry</p></Greet>
      // <Greet name="John">
      // <p>This is child of John</p></Greet>
      }<h2>Stop Watch</h2>
      <Counter/>
    </div>
  );
}

export default App;
